# def softmax_loss(self, labels, logits):
#     labels = tf.reshape(labels, shape=[-1, 1])
#
#     return tf.nn.sampled_softmax_loss(
#         # 其实就是接了一个全连接层
#         weights=tf.get_variable(name='softmax_w'),
#         biases=tf.get_variable(name='softmax_b'),
#         labels=labels,
#         inputs=logits,
#         num_sampled=512,
#         num_classes=self.vocab_size,
#     )
