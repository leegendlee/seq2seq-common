from utils import *

base_path = os.getcwd()
data_dir = os.path.join(base_path, 'data')

if __name__ == '__main__':
    with tf.Session() as sess:
        # file = open(os.path.join(data_dir, 'prisonb.txt'), 'r', encoding='UTF-8')
        # lines = file.readlines()
        # x_data_arr, y_data_arr = [], []
        # for i in range(len(lines)):
        #     line = lines[i]
        #     line = line.strip('\r\n').lstrip('M').lstrip('E').strip('E').strip('\ufeff').strip()
        #     if len(line) == 0 or line == ' ':
        #         continue
        #     line += '\r\n'
        #     if i % 2 == 0:
        #         x_data_arr.append(line)
        #     elif i % 2 != 0:
        #         y_data_arr.append(line)
        # if len(x_data_arr) < len(y_data_arr):
        #     y_data_arr.pop()
        # elif len(y_data_arr) < len(x_data_arr):
        #     x_data_arr.pop()
        # file.close()
        # x_data, y_data = '', ''
        # for x_line in x_data_arr:
        #     x_data += x_line
        # for y_line in y_data_arr:
        #     y_data += y_line
        # x_data_txt = open(os.path.join(data_dir, 'x_train.txt'), 'w', encoding='UTF-8')
        # y_data_txt = open(os.path.join(data_dir, 'y_train.txt'), 'w', encoding='UTF-8')
        # x_data_txt.write(x_data)
        # y_data_txt.write(y_data)
        # x_data_txt.close()
        # y_data_txt.close()
        model = Seq2seq(time_series=False, sess=sess, data_dir=data_dir).model
        model.create_dict(['x_train.txt', 'y_train.txt'])
        model.create_seq(['x_train.txt', 'y_train.txt'])
        condition = 'predict'
        if condition == 'train':
            x_train = open(os.path.join(data_dir, 'new_x_train.txt'), encoding='UTF-8').readlines()
            y_train = open(os.path.join(data_dir, 'new_y_train.txt'), encoding='UTF-8').readlines()
            model.train(x_train=x_train, y_train=y_train, batch_size=100, epoch=100000, dropout=0.8)
        else:
            sys.stdout.write('> ')
            sys.stdout.flush()
            line = sys.stdin.readline()
            while line:
                line = line.strip('').strip('\r\n')
                x_predict = []
                if len(line) == 0:
                    print('Please input words...')
                else:
                    tokens = jieba.cut(line)
                    not_found = False
                    for token in tokens:
                        word_id = model.dictionary.word2id.get(token)
                        if word_id is None:
                            not_found = True
                            break
                        x_predict.append(str(word_id))
                    if not_found is True:
                        print('Words not in dict...')
                    else:
                        x_predict = ' '.join(x_predict)
                        print(model.predict(x_predict=[x_predict], dropout=1))
                sys.stdout.write('> ')
                sys.stdout.flush()
                line = sys.stdin.readline()
