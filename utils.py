from modules import *


class Dictionary:
    def __init__(self, data_dir=None):
        self.data_dir = data_dir
        self._PAD = 'PAD'
        self._PAD_ID = 0
        self._BEG = 'BEG'
        self._BEG_ID = 1
        self._EOS = 'EOS'
        self._EOS_ID = 2
        self._UNK = 'UNK'
        self._UNK_ID = 3
        self.word2id = None
        self.id2word = None
        self.num_dict = 0

    def create_dict(self, file_names=('x_train.txt', 'y_train.txt')):
        if self.data_dir is None:
            raise Exception('Invalid Data Dir for Dictionary')
        words = set()
        files = []
        for file_name in file_names:
            files.append(os.path.join(self.data_dir, file_name))
        for path in files:
            file = open(path, 'r', encoding='UTF-8')
            lines = file.readlines()
            for line in lines:
                line = line.strip('\r\n').strip('')
                tokens = jieba.cut(line)
                for token in tokens:
                    words.add(token)
            file.close()
        word2id, id2word = {}, {}
        i = 4
        for word in words:
            word2id[word] = i
            id2word[i] = word
            i += 1
        dictionary = open(os.path.join(self.data_dir, 'dict.txt'), 'w', encoding='UTF-8')
        for (k, v) in word2id.items():
            dictionary.write(str(k) + ': ' + str(v) + '\r\n')
        dictionary.close()
        self.word2id, self.id2word, self.num_dict = word2id, id2word, len(word2id)
        return word2id, id2word

    def create_seq(self, file_names=('x_train.txt', 'y_train.txt')):
        word2id, _ = self.create_dict()
        for file_name in file_names:
            file = open(os.path.join(self.data_dir, file_name), 'r', encoding='UTF-8')
            lines = file.readlines()
            seqs = []
            for line in lines:
                line = line.strip('').strip('\r\n')
                if len(line):
                    tokens = jieba.cut(line)
                    sub_seqs = []
                    for token in tokens:
                        sub_seqs.append(str(word2id.get(token)))
                    if 'y' in file_name:
                        sub_seqs.insert(0, str(self._BEG_ID))
                        sub_seqs.append(str(self._EOS_ID))
                    seqs.append(sub_seqs)
            seqs = [' '.join(seq) + '\r\n' for seq in seqs]
            file.close()
            dictionary = open(os.path.join(self.data_dir, 'new_' + file_name), 'w', encoding='UTF-8')
            dictionary.writelines(seqs)
            dictionary.close()

    @property
    def BEG_ID(self):
        return self._BEG_ID

    @property
    def EOS_ID(self):
        return self._EOS_ID

    @property
    def PAD_ID(self):
        return self._PAD_ID


class ModelNLP:
    def __init__(self, sess=None, data_dir=None, num_units=256, num_layers=3):
        self.sess = sess
        self.dictionary = Dictionary(data_dir)
        self.vocab_size = 0
        self.encoder_inputs = []
        self.decoder_inputs = []
        self.target_weights = []
        self.targets = []
        self.num_units = num_units
        self.num_layers = num_layers
        self.outputs = None
        self.saver = None

    def create_dict(self, file_names):
        self.dictionary.create_dict(file_names)
        self.vocab_size = self.dictionary.num_dict

    def create_seq(self, file_names):
        self.dictionary.create_seq(file_names)

    def preprocess_from_txt(self, seqs, is_y=False):
        _seqs = []
        # 聚类得到桶的长度集，y的桶长度怎么和x关联起来？
        # for seq in seqs:
        #     seq = seq.strip('').strip('\r\n')
        buckets = [10, 20]
        for seq in seqs:
            seq = seq.strip('').strip('\r\n')
            if len(seq) == 0:
                continue
            seq = seq.split(' ')
            for bucket in buckets:
                delta = len(seq) - bucket
                if delta < 0:
                    for i in range(abs(delta)):
                        if is_y is False:
                            seq.append(str(self.dictionary.PAD_ID))
                        else:
                            seq.insert(-1, str(self.dictionary.PAD_ID))
                    break
            _seqs.append(np.array(seq, dtype=np.int32))
        return np.array(_seqs), buckets

    def model_seq2seq(self, encoder_inputs, decoder_inputs, dropout=0.8, feed_previous=False):
        vocab_size = self.vocab_size + 5  # ...多加了个1？
        with tf.variable_scope('seq2seq') as scope:
            cell = rnn.DropoutWrapper(rnn.GRUCell(self.num_units), input_keep_prob=dropout,
                                      output_keep_prob=dropout)
            # cell = tf.get_variable(name='cell', shape=gru.shape, initializer=gru)

            # cell_fw = rnn.DropoutWrapper(rnn.GRUCell(self.num_units), input_keep_prob=dropout,
            #                              output_keep_prob=dropout)
            # cell_bw = rnn.DropoutWrapper(rnn.GRUCell(self.num_units), input_keep_prob=dropout,
            #                              output_keep_prob=dropout)
            # = tf.nn.bidirectional_dynamic_rnn(cell_fw=cell_fw, cell_bw=cell_bw, inputs=[100, 20, self.num_units])
            if self.num_layers > 1:
                cell = rnn.MultiRNNCell(
                    [rnn.DropoutWrapper(rnn.GRUCell(self.num_units), input_keep_prob=dropout,
                                        output_keep_prob=dropout)
                     for _ in range(self.num_layers)])
                # w = tf.get_variable(name='output_w', shape=[self.num_units, vocab_size], dtype=tf.float32),
                # b = tf.get_variable(name='output_b', shape=[vocab_size], dtype=tf.float32)
                # output_projection的维数有问题。。。
        model_seq2seq = legacy_seq2seq.embedding_attention_seq2seq(
            encoder_inputs=encoder_inputs,
            decoder_inputs=decoder_inputs,
            cell=cell,
            num_encoder_symbols=vocab_size,
            num_decoder_symbols=vocab_size,
            embedding_size=self.num_units,
            feed_previous=feed_previous,
        )
        return model_seq2seq

    def model(self, dropout=0.8, buckets=(), feed_previous=False):
        for i in range(buckets[-1][0]):
            self.encoder_inputs.append(tf.placeholder(dtype=tf.int32, shape=[None], name='encoder_inputs{0}'.format(i)))
        for i in range(buckets[-1][1] + 1):
            self.decoder_inputs.append(tf.placeholder(dtype=tf.int32, shape=[None], name='decoder_inputs{0}'.format(i)))
            self.target_weights.append(
                tf.placeholder(dtype=tf.float32, shape=[None], name='target_weights{0}'.format(i)))
        for i in range(len(self.decoder_inputs) - 1):
            self.targets.append(self.decoder_inputs[i + 1])

        outputs, losses = legacy_seq2seq.model_with_buckets(encoder_inputs=self.encoder_inputs,
                                                            decoder_inputs=self.decoder_inputs,
                                                            targets=self.targets, weights=self.target_weights,
                                                            buckets=buckets,
                                                            seq2seq=lambda x, y: self.model_seq2seq(x, y, dropout,
                                                                                                    feed_previous))
        return outputs, losses

    def get_batch(self, x_train, y_train, buckets, batch_size=100, is_random=True):
        x_batch, y_batch, target_weights_batch = [], [], []
        size = min([len(x_train), len(y_train)])
        bucket_idx = random.choice(range(len(buckets)))
        x_bucket, y_bucket = buckets[bucket_idx]
        count = 0
        while count < batch_size:
            if is_random:
                idx = random.choice(range(size))
                x_temp, y_temp = x_train[idx], y_train[idx]
            else:
                idx = 0
                x_temp, y_temp = x_train[idx], y_train[idx]
                bucket_idx = buckets.index([len(x_temp), len(y_temp)])
                x_bucket, y_bucket = buckets[bucket_idx]
            if len(x_temp) == x_bucket and len(y_temp) == y_bucket:
                target_weight = []
                for y in y_temp:
                    target_weight.append(
                        0.0 if y == self.dictionary.BEG_ID or y == self.dictionary.PAD_ID or y == self.dictionary.EOS_ID else 1.0)
                x_batch.append(x_temp)
                y_batch.append(y_temp)
                target_weights_batch.append(target_weight)
                count += 1
        return np.transpose(np.array(x_batch)), np.transpose(np.array(y_batch)), np.transpose(
            np.array(target_weights_batch)), bucket_idx

    def get_batch_from_random(self, buckets, batch_size=100):
        x_batch, y_batch, target_weights_batch = [], [], []
        bucket_idx = random.choice(range(len(buckets)))
        x_bucket, y_bucket = buckets[bucket_idx]
        count = 0
        ids = list(self.dictionary.id2word.keys())
        while count < batch_size:
            x_temp, y_temp = [], []
            for x in range(x_bucket):
                x_id = random.choice(ids)
                x_temp.append(x_id)
            for y in range(y_bucket):
                y_id = random.choice(ids)
                y_temp.append(y_id)
            if len(x_temp) == x_bucket and len(y_temp) == y_bucket:
                target_weight = []
                for y in y_temp:
                    target_weight.append(0.0 if (
                        y == self.dictionary.BEG_ID or y == self.dictionary.PAD_ID or y == self.dictionary.EOS_ID) else 1.0)

                # 没有加入标识
                # print(x_temp)
                # print(y_temp)
                # print(target_weight)
                x_batch.append(x_temp)
                y_batch.append(y_temp)
                target_weights_batch.append(target_weight)
                count += 1
        return np.transpose(np.array(x_batch)), np.transpose(np.array(y_batch)), np.transpose(
            np.array(target_weights_batch)), bucket_idx

    def train(self, x_train, y_train, batch_size=100, epoch=10000, dropout=0.8):
        global_step = tf.Variable(initial_value=1000, trainable=False, dtype=tf.int32)
        x_train, x_buckets = self.preprocess_from_txt(x_train)
        y_train, y_buckets = self.preprocess_from_txt(y_train, is_y=True)
        buckets = []
        for i in range(len(x_buckets)):
            bucket = [x_buckets[i], y_buckets[i]]
            buckets.append(bucket)
        # buckets = [[10, 10], [15, 15]]
        outputs, losses = self.model(dropout, buckets)
        params = tf.trainable_variables()
        optimizer = tf.train.AdamOptimizer()
        updates = []
        for bucket_idx in range(len(buckets)):
            gradients = tf.gradients(losses[bucket_idx], params)
            clipped_gradients, _ = tf.clip_by_global_norm(gradients, clip_norm=5.0)
            updates.append(optimizer.apply_gradients(zip(clipped_gradients, params), global_step=global_step))
        saver = tf.train.Saver()
        self.sess.run(tf.global_variables_initializer())
        for step in range(1, epoch + 1):
            x_train_batch, y_train_batch, target_weights_train_batch, bucket_idx = self.get_batch(x_train, y_train,
                                                                                                  buckets, batch_size)

            # x_train_batch, y_train_batch, target_weights_train_batch, bucket_idx = self.get_batch_from_random(buckets,
            #                                                                                                   batch_size)
            train_feed = {}
            train_ops = [losses[bucket_idx], updates[bucket_idx]]
            for i in range(buckets[bucket_idx][0]):
                train_feed[self.encoder_inputs[i].name] = x_train_batch[i]
            for i in range(buckets[bucket_idx][1]):
                train_feed[self.decoder_inputs[i].name] = y_train_batch[i]
                train_feed[self.target_weights[i].name] = target_weights_train_batch[i]
            train_feed[self.decoder_inputs[buckets[bucket_idx][1]].name] = np.zeros(shape=[batch_size], dtype=np.int32)

            current_loss, current_update = self.sess.run(train_ops, feed_dict=train_feed)
            print('Loss: ', current_loss)
            print('Epoch: ', step)
            if step % 500 == 0:
                saver.save(self.sess, 'ckpt_new/train-' + str(step) + '.ckpt', global_step=step)
                test_batch = 10
                x_test_batch, y_test_batch, target_weights_test_batch, bucket_idx = self.get_batch(x_train, y_train,
                                                                                                   buckets,
                                                                                                   test_batch)
                #
                # x_test_batch, y_test_batch, target_weights_test_batch, bucket_idx = self.get_batch_from_random(buckets,
                #                                                                                                test_batch)
                test_feed = {}
                test_ops = [losses[bucket_idx], outputs[bucket_idx]]

                for i in range(buckets[bucket_idx][0]):
                    test_feed[self.encoder_inputs[i].name] = x_test_batch[i]
                for i in range(buckets[bucket_idx][1]):
                    test_feed[self.decoder_inputs[i].name] = y_test_batch[i]
                    test_feed[self.target_weights[i].name] = target_weights_test_batch[i]
                test_feed[self.decoder_inputs[buckets[bucket_idx][1]].name] = np.zeros(shape=[test_batch],
                                                                                       dtype=np.int32)
                _, current_output = self.sess.run(test_ops, feed_dict=test_feed)
                # with tf.variable_scope('seq2seq', reuse=True) as scope:
                #     output_w, output_b = tf.get_variable(name='output_w'), tf.get_variable(name='output_b')
                current_output = np.array(current_output)
                y_test_batch = tf.transpose(y_test_batch)
                true_count = 0.0
                for i in range(test_batch):
                    y_test = tf.cast(y_test_batch[i], dtype=tf.int32)
                    # one_hot_matrix = tf.matmul(current_output[:, i, :], output_w) + output_b
                    one_hot_matrix = current_output[:, i, :]
                    word_ids = tf.cast(tf.argmax(one_hot_matrix, axis=1), dtype=tf.int32)
                    print('Original: ', y_test.eval())
                    print('Predicte: ', word_ids.eval())
                    if y_test.shape[0] == word_ids.shape[0] and tf.reduce_sum(
                            tf.cast(tf.equal(y_test, word_ids), dtype=tf.int32)).eval() == y_test.shape[0]:
                        true_count += 1.0
                    print('Accuracy: ', true_count / test_batch)

    def predict(self, x_predict, dropout=1):
        print('Encoder: ', x_predict)
        x_predict, x_buckets = self.preprocess_from_txt(x_predict)
        y_predict, y_buckets = x_predict, x_buckets
        buckets = []
        for i in range(len(x_buckets)):
            bucket = [x_buckets[i], y_buckets[i]]
            buckets.append(bucket)
        if self.outputs is None:
            self.outputs, _ = self.model(dropout, buckets, feed_previous=True)
        outputs = self.outputs
        if self.saver is None:
            self.saver = tf.train.Saver()
            self.saver.restore(self.sess, save_path='ckpt_new/train-29500.ckpt-29500')
        saver = self.saver
        self.sess.run(tf.global_variables_initializer())
        predict_batch = 1
        x_test_batch, y_test_batch, target_weights_test_batch, bucket_idx = self.get_batch(x_predict, y_predict,
                                                                                           buckets,
                                                                                           predict_batch,
                                                                                           is_random=False)
        predict_feed = {}
        predict_ops = [outputs[bucket_idx]]

        for i in range(buckets[bucket_idx][0]):
            predict_feed[self.encoder_inputs[i].name] = x_test_batch[i]
        for i in range(buckets[bucket_idx][1]):
            predict_feed[self.decoder_inputs[i].name] = y_test_batch[i]
            predict_feed[self.target_weights[i].name] = target_weights_test_batch[i]
            predict_feed[self.decoder_inputs[buckets[bucket_idx][1]].name] = np.zeros(shape=[predict_batch],
                                                                                      dtype=np.int32)
        print('Start Predicting...')
        current_output = self.sess.run(predict_ops, feed_dict=predict_feed)
        current_output = np.array(current_output[0])

        ret = []
        for i in range(predict_batch):
            one_hot_matrix = current_output[:, i, :]
            print('Number of words: ', self.dictionary.num_dict)
            print(one_hot_matrix)
            word_ids = tf.cast(tf.argmax(one_hot_matrix, axis=1), dtype=tf.int32).eval()
            print('Predicted: ', word_ids)
            current_sentence = ''
            for word_id in word_ids:
                if word_id not in [0, 1, 2]:
                    current_sentence += self.dictionary.id2word.get(word_id)
            ret.append(current_sentence)
        return ret[0]


class ModelTimeSeries:
    def __init__(self, sess=None, data_dir=None):
        self.sess = sess
        self.data_dir = data_dir


class Seq2seq:
    def __init__(self, time_series=False, sess=None, data_dir=None):
        self.model = ModelNLP(sess=sess, data_dir=data_dir) if time_series is False else ModelTimeSeries(sess=sess,
                                                                                                         data_dir=data_dir)
